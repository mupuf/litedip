# LiteX-based Discoverable IP Blocks

Plug-and-play LiteX-based IP blocks enabling the creation of generic Linux
drivers. Design your FPGA-based SoC with them and get a (potentially
upstream-able) driver for it instantly!

## Rationale

FPGAs have become very affordable, and tools like LiteX allow designing new SoCs
in a matter of hours. The problems come when wanting to interface the FPGA with
a more traditional computer as the SoC does not expose enough information to let
generic drivers expose its functionalities and custom drivers need to written.

For example, simple functions like thermal probes would always get exposed at
different offsets on every SoC (or even revisions), and the formula to get the
actual temperature in Kelvins would need to be written in the driver. LiteX
partially "works around" this issue by generating C header files containing the
addresses of all registers, but instead of having a plug-and-play SoC, you get
to recompile the driver and hope that the driver is in sync with the current
design.

Instead, an SoC could expose at a fixed address the list of available blocks,
identified by a 24-bit tag, a 8-bit version, and a 32-bit address. The tag
specifies what the block is about (temperature sensor, fan, video-in/out, ...),
the version specifies the major revision of the block, and the address specifies
where to find it on the bus.

Each block can then specify more information starting from their base address:

|   Address   |  Size | Description |
| ----------- |  ---- | ----------- |
| base + 0x00 |   32  | Minor version (to be incremented every time a change is made) |
| base + 0x04 |   32  | Backward compatible to version (best effort) |
| base + 0x08 |  ...  | More registers related to the block |

Additional information could be what unit the sensor is exposing the data in,
what is the number of usable bits in a PWM controlers along with the input
clock's frequency, or it could be another list of sub-blocks to allow for a
hierarchical design.

With such a design, a generic driver could be implemented that would read the
available blocks, then instanciate the corresponding software block that would
expose it to the userspace. For instance, a sensors could be detected and
exposed using lm_sensors on Linux without having to write a single line of code.

Now, need to disable a block to save space? No need to change the driver, it
will not find the block anymore and thus won't be trying to use it! Is that
convincing-enough?

## Frequently-asked questions

### This is a waste of gates!?

It definitely moves away from the traditional model where the hardware is as
simple as possible and the complexity is put mostly in the driver. Instead of
forcing the driver to build giant tables stating which IP is present on which
SoC version, we want to force the SoC to expose this information directly.

This model is more suitable to FPGA-based designs which allow for a more
iterative design, and where having a backward compatible design helps customers
update their SoC and associated driver more freely.

In the end, we believe that the amount of gates needed to make blocks
discoverable are not significant compared to the added benefits. There are often
a lot of unneeded complexity in the hardware which could be dropped without
affecting users in any way, and enforcing this co-design will help save enough
gates to pay for itself.

### Why not auto-generate a device-tree file instead?

The role of the device tree is very much to document non-discoverable blocks.
It is the dominant way to describe complete SoCs in the Linux world, and it has
been proven to be sufficient in real life.

The main differences between this project and the device tree are:

 1. Device tree require shipping a file describing the HW to all users (or
    store in a ROM).
 1. Device trees are aimed at describing the SoC Linux is running on, rather
    than a hot-pluggeable device.

It is thus theoritically possible to use a device tree to represent
hot-pluggeable devices, but it complicates the process of writing independant
drivers for HW blocks as they would need to be given all the instance's
parameters while being instanciated in the driver's side.

This sounds more complicated from a driver's perspective as simply being given
a base pointer and then reading from there all the necessary information.
being given a pointer to 

### How can I add a new block to this collection?

We strongly believe in the concept of co-design. Solve problems where it is
easiest to get solved, keep the overall code base as maintainable as possible.

This is why new blocks will need to be accompanied by their driver, so as to
make review possible and serve as a reference implementation which may be
directly copied in the final driver as needed.

We accept contributions through merge requests on this project.

### Can you find a better name for the project?

Yes, we'll try our best!

### I have a question not answered here, where can I contact you?

Please create an issue in this project and I will make sure to answer the
question there, and maybe move it to this README.
